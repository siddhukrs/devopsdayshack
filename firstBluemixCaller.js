'use strict';

var querystring = require('querystring');
var unirest = require("unirest");
var aws = require('aws-sdk');

var lambda = new aws.Lambda({
  region: 'us-east-1'
});

function httpCallback(data, callback)
{
    console.log("inputdata - " + JSON.stringify(data));
    unirest.post('https://ibm-watson-ml.mybluemix.net/score/plsworkgod_v2?accesskey=BMy6wS44w/qmrNBt/sDNFjTu6jnmFEu2uWG/5QtGXDjWTl2F/2Urc3zT9uaqnjO/HxGxQ3pIogjgEOjN0TGDTcL0h32gVzPkwMbmHXNpi+FQYUqQmv73SQJrb1WXWeZv')
        .headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
        .send(JSON.stringify(data))
        .end(function (response) {
          callback(null, response.body);
        });
}

exports.handler = (event, context, callbackMain) => {
    httpCallback(event, function(error, body){
        if (error) 
        {
            context.done('error', error);
        }
        else
        {
            var jsonResponse = body;
            var arr = jsonResponse[0]["data"][0];
            var isThreat = arr[9];
            if(isThreat == 1)
            {
                // needs additional classification
                console.log("making second call to classify threat because " + jsonResponse[0]["header"][9] );
                lambda.invoke({
                  FunctionName: 'secondBluemixCaller',
                  Payload: JSON.stringify(event)
                }, function(moreerror, data) {
                  if (moreerror) {
                    console.log(moreerror);
                    context.done('error','error from bluemix call 2: '+moreerror);
                  }
                  if(data.Payload){
                    console.log("outputdata - " + JSON.stringify(data.Payload));
                   context.succeed(data.Payload)
                  }
                });
            }
            else
            {
                // done classifying. this is not a threat
                console.log("outputdata - " + JSON.stringify(body));
                context.succeed(body);
            }
            
        }
    });
};