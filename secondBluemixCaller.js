'use strict';

var querystring = require('querystring');
var unirest = require("unirest");

function httpCallback(data, callback)
{
  data["tablename"] = "output_finalreqd_data_v2.csv";
  console.log("inputdata - " + JSON.stringify(data));
    unirest.post('https://ibm-watson-ml.mybluemix.net/score/plsworkgod_v4?accesskey=BMy6wS44w/qmrNBt/sDNFjTu6jnmFEu2uWG/5QtGXDjWTl2F/2Urc3zT9uaqnjO/HxGxQ3pIogjgEOjN0TGDTcL0h32gVzPkwMbmHXNpi+FQYUqQmv73SQJrb1WXWeZv')
        .headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
        .send(JSON.stringify(data))
        .end(function (response) {
          callback(null, response.body);
        });
}

exports.handler = (event, context, callbackMain) => {
    httpCallback(event, function(error, body){
        if (error) 
        {
          console.log(error);
            context.done('error','error from bluemix call 2: '+error);
        }
        else
        {
          console.log("outputdata - " + JSON.stringify(body));
          context.succeed(body);            
        }
    });
};