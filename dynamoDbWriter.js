var AWS = require('aws-sdk');

var dynamodb = new AWS.DynamoDB();

exports.handler = function(event, context) {
    var tableName = "threatops";
    dynamodb.putItem({
        "TableName": tableName,
        "Item" : {
            "id": {N: '4'},
            "name": {S: "Bar"}
        }
    }, function(err, data) {
        console.log(err + "here");
        if (err) {
            context.done('error','putting item into dynamodb failed: '+err);
        }
        else {
            console.log('great success: '+JSON.stringify(data, null, '  '));
            context.succeed("Successfully Inserted");
        }
    });
};