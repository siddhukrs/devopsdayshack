'use strict';

var http = require("http");
var querystring = require('querystring');
var aws = require('aws-sdk');
var zlib = require('zlib');

var lambda = new aws.Lambda({
  region: 'us-east-1'
});

var normalizedDataFormat = {"tablename":"output_finalreqd_data_v2test.csv","header":["id","sttl","dload","swin","dmean","trans_depth","ct_state_ttl","ct_dst_src_ltm","ct_srv_dst","label","spkts_LogN","sbytes_LogN","dbytes_LogN","rate_LogN","sload_LogN","sloss_LogN","dloss_LogN","sinpkt_LogN","sjit_LogN","djit_LogN","stcpb_LogN","dtcpb_LogN","synack_LogN","smean_LogN","response_body_len_LogN","service_http","Partition"],"data":[]};

function httpCallback(data, callback)
{
    var url = "http://ec2-54-91-68-124.compute-1.amazonaws.com:3030/normalize?id=" + data.id;
    var req = http.get(url, function (response) {
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {
            callback(JSON.parse(body));
        });
    });
    req.end();
}

exports.handler = (event, context, callbackMain) => {

    var payload = new Buffer(event.awslogs.data, 'base64');
    zlib.gunzip(payload, (err, res) => {
        if (err) {
            return callback(err);
        }
        const parsed = JSON.parse(res.toString('utf8'));
        
        var unNormalizedData = JSON.parse(parsed.logEvents[0].message);
        console.log("unnormalizedData : " + JSON.stringify(unNormalizedData));
        
        httpCallback(unNormalizedData, function(res){
            normalizedDataFormat.data = [];
            normalizedDataFormat.data.push(getValues(res)); 
            lambda.invoke({
              FunctionName: 'firstBluemixCaller',
              Payload: JSON.stringify(normalizedDataFormat)
            }, function(error, data) {
              if (error) {
                console.log(error);
                context.done('error','error fetching normalized data: '+error);
              }
              if(data.Payload){
                 console.log(data.Payload);
                 // context.succeed(data.Payload);
                 lambda.invoke({
                    FunctionName: 'putdynamodb',
                    Payload: JSON.stringify(getDataForDb(unNormalizedData, data.Payload))
                }, function(moreerror, moredata) {
                    if (moreerror) {
                        console.log(moreerror);
                      context.done('error','error from db call 1: '+moreerror);
                    }
                    if(moredata.Payload){
                        console.log(moredata.Payload);
                        context.succeed(moredata.Payload)
                    }
                });
             }
            });
        });
    });
};

function attachHeaders(json)
{
    var obj = {};
    var parsedJson = JSON.parse(json);
    console.log("attachheaders - " + json);
    console.log("zero - " + parsedJson[0]);
    console.log("headers - " + parsedJson[0]["header"]);
    var headers = parsedJson[0].header;
    console.log("data - " + parsedJson[0]["data"]);
    var data = parsedJson[0].data[0];
    for(var i = 0; i < headers.length; i++)
    {
        obj[headers[i]] = data[i];
    }
    return obj;
}

function getDataForDb(oldJson, newJson)
{
    console.log(oldJson);
    console.log(JSON.parse(newJson));
    console.log(newJson);
    newJson = attachHeaders(JSON.parse(newJson));

    var expectedData = {
      "ackdat": {
        "N": oldJson["ackdat"]
      },
      "attack_cat": {
        "S": newJson["$R-attack_cat_no"]
      },
      "ct_dst_ltm": {
        "N": oldJson["ct_dst_ltm"]
      },
      "ct_dst_sport_ltm": {
        "N": oldJson["ct_dst_sport_ltm"]
      },
      "ct_dst_src_ltm": {
        "N": oldJson["ct_dst_src_ltm"]
      },
      "ct_flw_http_mthd": {
        "N": oldJson["ct_flw_http_mthd"]
      },
      "ct_ftp_cmd": {
        "N": oldJson["ct_ftp_cmd"]
      },
      "ct_src_dport_ltm": {
        "N": oldJson["ct_src_dport_ltm"]
      },
      "ct_src_ltm": {
        "N": oldJson["ct_src_ltm"]
      },
      "ct_srv_dst": {
        "N": oldJson["ct_srv_dst"]
      },
      "ct_srv_src": {
        "N": oldJson["ct_srv_src"]
      },
      "ct_state_ttl": {
        "N": oldJson["ct_state_ttl"]
      },
      "dbytes": {
        "N": oldJson["dbytes"]
      },
      "dinpkt": {
        "N": oldJson["dinpkt"]
      },
      "djit": {
        "N": oldJson["djit"]
      },
      "dload": {
        "N": oldJson["dload"]
      },
      "dloss": {
        "N": oldJson["dloss"]
      },
      "dmean": {
        "N": oldJson["dmean"]
      },
      "dpkts": {
        "N": oldJson["dpkts"]
      },
      "dtcpb": {
        "N": oldJson["dtcpb"]
      },
      "dttl": {
        "N": oldJson["dttl"]
      },
      "dur": {
        "N": oldJson["dur"]
      },
      "dwin": {
        "N": oldJson["dwin"]
      },
      "is_ftp_login": {
        "N": oldJson["is_ftp_login"]
      },
      "is_sm_ips_ports": {
        "N": oldJson["is_sm_ips_ports"]
      },
      "label": {
        "N": newJson["label"].toString()
      },
      "logid": {
        "N": oldJson["id"]
      },
      "proto": {
        "S": oldJson["proto"]
      },
      "rate": {
        "N": oldJson["rate"]
      },
      "response_body_len": {
        "N": oldJson["response_body_len"]
      },
      "sbytes": {
        "N": oldJson["sbytes"]
      },
      "service": {
        "S": oldJson["service"]
      },
      "sinpkt": {
        "N": oldJson["sinpkt"]
      },
      "sjit": {
        "N": oldJson["sjit"]
      },
      "sload": {
        "N": oldJson["sload"]
      },
      "sloss": {
        "N": oldJson["sloss"]
      },
      "smean": {
        "N": oldJson["smean"]
      },
      "spkts": {
        "N": oldJson["spkts"]
      },
      "state": {
        "S": oldJson["state"]
      },
      "stcpb": {
        "N": oldJson["stcpb"]
      },
      "sttl": {
        "N": oldJson["sttl"]
      },
      "swin": {
        "N": oldJson["swin"]
      },
      "synack": {
        "N": oldJson["synack"]
      },
      "tcprtt": {
        "N": oldJson["tcprtt"]
      },
      "timestamp": {
        "S": new Date().getTime().toString()
      },
      "trans_depth": {
        "N": oldJson["trans_depth"]
        }
    };
  return expectedData;
}

function getValues(obj)
{
    var returnObj = [];
    for (var name in obj) 
    {
        if (obj.hasOwnProperty(name)) 
        {
            if(obj[name] == "")
            {
                returnObj.push(null);
            }
            else
            {
                returnObj.push(obj[name]);
            }
        }
    }
    return returnObj;
}